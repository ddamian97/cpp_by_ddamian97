//
//  main.cpp
//  cwiczenia2_z1
//
//  Created by Damian Dudek on 10.10.2018.
//  Copyright © 2018 Damian Dudek. All rights reserved.
//

/*Zad. 1 (do 17.10)
 Klasa Samochod (poziom dostępu tylko public):
 a) Pola: stan (silnik włączony czy nie), paliwo, spalanie (trzeba sobie jakieś przyjąć),
 pojemnosc_baku, przebieg, … (ewentualnie inne potrzebne).
 
 b) Metody: uruchom_silnik(), zatrzymaj_silnik(), jedz(N km), tankuj(N litrow), prezentuj()
 (wyświetla wartości wszystkich pol klasy)
 
 
 W klasie należy obsłużyć wszystkie sytuacje specjalne (np. samochód nie może zostać zatankowany
 powyżej pojemności baku). Jeśli coś nie jest możliwe np. wywołanie jedz() bez uruchomionego silnika
 lub z niewystarczającą ilością paliwa, to komunikat błędu na wyjściu lub wykonanie części możliwej
 operacji (np. przejazd tylko takiego kawałka żądanej trasy, na który starczy paliwa).
 Napisz program demonstracyjny ilustrujący działanie zaimplementowanej klasy (proste menu z możliwością wyboru i uruchomienia zaimplementowanych w klasie metod).*/

//stworzyc klase, dostep tylko public wystarczy, jak sie tworzy klase i wpisze public to public, jak nic to bedzie private. Pola: stan - silnik wlaczony lub nie (0,1 lub true false). Paliwo - ilosc paliwa w danym momencie. spalanaie - trzeba wyliczyc. Pojemnosc, przebieg  - dodajemy dystans. Inne - chyba nie trzeba.
//metody - uruhcom silnik - wlacza pole IOPOL_STANDARDzatrzymaj silnik - odwrotnie
//jedz - podajemy ilosc kilometrow, odliczamy ilosc paliwa, dodajemy przebieg i liczymy spalanie.
//tankuj - n litrow - aktualizujemy ilosc paliwa,
//prezentuj - wyswietla przebieg, paliwo itd.

//UWAGA - ma byc klasa spojna i logiczna - nie mozna aby pola klasy mialy nieprawidlowe wartosci, logika jazdy musi byc pelna spojna.
// Jesli ma przejechac n kilometrow - sprawdzamy czy silnik wlaczony, podajemy komunikat. sprawdzamy na ile wystarczy paliwa - wystarczy lub nie, komunikat

// tankuj - sprawdza czy sie zmiesci tyle paliwa

///proba odpalenia silnika drugi raz lub odpalenie bez paliwa

#include <iostream>

class samochod
{
public:
    
    int stan=0, pojemnosc_baku=60;
    double paliwo=60.0, spalanie=7.5, przebieg=1000.0, kilometry;
    
    int wlacz_silnik()
    {
        if (stan==1)
        {
            std::cout<<" SILNIK JUZ PRACUJE!\n";
            return 1;
        }
        stan=1;
        std::cout<<"Silnik uruchomiony\n";
        return 1;
    }
    
    int wylacz_silnik()
    {
        if (stan==0)
        {
            std::cout<<"SILNIK NIE JEST URUCHOMIONY!!\n";
            return 0;
        }
        
        stan=0;
        std::cout<<"Silnik wylaczony\n";
        return 0;
    }
    
    void prezentuj()
    {
        if(stan==0)
        {
            std::cout<<"\nSilnik: wylaczony";
            
        }
        if(stan==1)
        {
            std::cout<<"\nSilnik: wlaczony";
        }
        std::cout<<"\nPojemnosc baku (l): "<<pojemnosc_baku;
        std::cout<<"\nIlosc paliwa (l): "<<paliwo;
        std::cout<<"\nSrednie spalanie (l/100km): "<<spalanie;
        std::cout<<"\nPrzebieg (km): "<<przebieg;
        
        
        
        
    }
    
    int jedz(double kilometry)
    {
        
        double ile_przejade;
        ile_przejade= paliwo*100/spalanie;
        double pozostanie=0, spali=0, litry=0;
        
        
        if (ile_przejade<kilometry)
        {
            std::cout<<"\nZBYT MALA ILOSC PALIWA! ZATANKUJ PRZED WYJAZDEM!\n";
            return 0;
        }
        spali=kilometry*7.5/100;
        pozostanie=paliwo-spali;
        paliwo=pozostanie;
        przebieg+=kilometry;
        
        std::cout<<"\nPo podrozy zostanie Ci w baku "<<paliwo<<" litrow paliwa\n";
        std::cout<<"\nPo podrozy przebieg bedzie wynosil "<<przebieg<<" kilometrow\n\n\n";
        return 1;
    }
    
    int tankuj(double litry)
    {
        if (stan==1)
        {
            std::cout<<"\nNAJPIERW WYLACZ SILNIK!\n";
            return 0;
        }
        
        else if (litry+paliwo<=pojemnosc_baku)
        {
            paliwo+=litry;
            return 1;
            
        }
        
        else if(litry+paliwo>pojemnosc_baku)
        {
            std::cout<<"\nTYLE SIE NIE ZMIESCI\n";
            return 1;
        }
        return 0;
    }
    
    
    
    
};

int main(int argc, const char * argv[]) {
    // insert code here...
    std::cout << "\nMenu samochodu\n";
    
    samochod ds3;
    int wybor=0;
    
    while(wybor<9)
    {
        std::cout <<"\n\nCo chcesz zrobic?\n\n";
        std::cout<<"1. Uruchom silnik\n";
        
        std::cout<<"2. Sprawdz stan paliwa\n";
        std::cout<<"3. Sprawdz wielkosc zbiornika paliwa\n";
        std::cout<<"4. Sprawdz przebieg\n";
        std::cout<<"5. Jedz\n";
        std::cout<<"6. Tankuj\n";
        std::cout<<"7. Wylacz silnik\n";
        std::cout<<"8. Prezentuj\n";
        std::cout<<"9. wyjdz\n";
        
        
        std::cin>>wybor;
        
        switch( wybor )
        {
            case 1:
                ds3.wlacz_silnik();
                break;
                
                
            case 2:
                std::cout<<"\nStan paliwa: "<<ds3.paliwo<<"l";
                break;
                
            case 3:
                std::cout<<"\nWielkosc zbiornika wynosi: "<<ds3.pojemnosc_baku<<"l";
                break;
                
            case 4:
                std::cout<<"\nAktualny przebieg samochodu to: "<<ds3.przebieg<<"kilometrow";
                break;
                
            case 5:
                if (ds3.stan==0)
                {
                    std::cout<<"\nNAJPIERW URUCHOM SILNIK!!\n";
                    continue;
                }
                double km;
                std::cout<<"\nIle kilometrow masz zamiar zrobic?\n";
                std::cin>>km;
                ds3.jedz(km);
                
                break;
                
            case 6:
                if (ds3.stan==1)
                {
                    std::cout<<"NAJPIERW WYLACZ SILNIK!\n";
                    break;
                }
                double litry1;
                std::cout<<"\nW zbiorniku o pojemnosci "<<ds3.pojemnosc_baku<<" litrow znajduje sie "<<ds3.paliwo<<" litrow paliwa";
                std::cout<<"\nIle litrow chcesz zatankowac?\n";
                std::cin>>litry1;
                if((litry1+ds3.paliwo)>ds3.pojemnosc_baku)
                {
                    std::cout<<"\nTYLE SIE NIE ZMIESCI!\n";
                    break;
                }
                ds3.tankuj(litry1);
                std::cout<<"\nPo zatankowaniu w zbiorniku znajduje sie"<<ds3.paliwo<<" litrow paliwa\n";
                break;
                
            case 7:
                ds3.wylacz_silnik();
                break;
                
            case 8:
                ds3.prezentuj();
                
                break;
                
                
                
                
            default:
                std::cout<<"WYJSCIE\n";
                break;
        }
    }
    
    
    return 0;
}
