//
//  main.cpp
//  cwiczenia3_z1
//
//  Created by Damian Dudek on 17.10.2018.
//  Copyright © 2018 Damian Dudek. All rights reserved.
//
//Napisz klasę posiadającą konstruktor domyślny (bezparametrowy) oraz destruktor (można dopisać do
//tworzonej na poprzednie zadanie klasy Samochod lub stworzyć nową klasę). Konstruktor musi mieć
//przynajmniej jeden parametr domyślny ustawiający wartość jakiegoś pola klasy. Konstruktor i
//destruktor powinny wypisywać komunikat informujący o ich wywołaniu.
//Napisz program demonstracyjny ilustrujący działanie zaimplementowanej klasy z uwzględnieniem
//wywołania konstruktora i destruktora.


#include <iostream>

class samochod
{
public:
    
    
    int stan, pojemnosc_baku;
    double paliwo, spalanie, przebieg, kilometry;
    
    samochod(int spalanie1=7)
    {
        stan=0;
        pojemnosc_baku=60;
        paliwo=60.0;
        spalanie=spalanie1;
        przebieg=1000.0;
        
        std::cout <<"\nKonstruktor domyslny (bezparametrowy)\n";
    };
    
    
    int wlacz_silnik()
    {
        if (stan==1)
        {
            std::cout<<" SILNIK JUZ PRACUJE!\n";
            return 1;
        }
        stan=1;
        std::cout<<"Silnik uruchomiony\n";
        return 1;
    }
    
    int wylacz_silnik()
    {
        if (stan==0)
        {
            std::cout<<"SILNIK NIE JEST URUCHOMIONY!!\n";
            return 0;
        }
        
        stan=0;
        std::cout<<"Silnik wylaczony\n";
        return 0;
    }
    
    void prezentuj()
    {
        if(stan==0)
        {
            std::cout<<"\nSilnik: wylaczony";
            
        }
        if(stan==1)
        {
            std::cout<<"\nSilnik: wlaczony";
        }
        std::cout<<"\nPojemnosc baku (l): "<<pojemnosc_baku;
        std::cout<<"\nIlosc paliwa (l): "<<paliwo;
        std::cout<<"\nSrednie spalanie (l/100km): "<<spalanie;
        std::cout<<"\nPrzebieg (km): "<<przebieg;
        
        
        
        
    }
    
    int jedz(double kilometry)
    {
        
        double ile_przejade;
        ile_przejade= paliwo*100/spalanie;
        double pozostanie=0, spali=0, litry=0;
        
        
        if (ile_przejade<kilometry)
        {
            std::cout<<"\nZBYT MALA ILOSC PALIWA! ZATANKUJ PRZED WYJAZDEM!\n";
            return 0;
        }
        spali=kilometry*7.5/100;
        pozostanie=paliwo-spali;
        paliwo=pozostanie;
        przebieg+=kilometry;
        
        std::cout<<"\nPo podrozy zostanie Ci w baku "<<paliwo<<" litrow paliwa\n";
        std::cout<<"\nPo podrozy przebieg bedzie wynosil "<<przebieg<<" kilometrow\n\n\n";
        return 1;
    }
    
    int tankuj(double litry)
    {
        
        paliwo+=litry;
        return 1;
    }
    
    
   ~samochod()
    {
        std::cout<<"\nDestruktor\n";
    };
    
    
};

int main(int argc, const char * argv[]) {
    // insert code here...
    std::cout << "\nMenu samochodu\n";
    
    samochod ds3;
    int wybor=0;
    
    while(wybor<9)
    {
        std::cout <<"\n\nCo chcesz zrobic?\n\n";
        std::cout<<"1. Uruchom silnik\n";
        
        std::cout<<"2. Sprawdz stan paliwa\n";
        std::cout<<"3. Sprawdz wielkosc zbiornika paliwa\n";
        std::cout<<"4. Sprawdz przebieg\n";
        std::cout<<"5. Jedz\n";
        std::cout<<"6. Tankuj\n";
        std::cout<<"7. Wylacz silnik\n";
        std::cout<<"8. Prezentuj\n";
        std::cout<<"9. wyjdz\n";
        
        
        std::cin>>wybor;
        
        switch( wybor )
        {
            case 1:
                ds3.wlacz_silnik();
                break;
                
                
            case 2:
                std::cout<<"\nStan paliwa: "<<ds3.paliwo<<"l";
                break;
                
            case 3:
                std::cout<<"\nWielkosc zbiornika wynosi: "<<ds3.pojemnosc_baku<<"l";
                break;
                
            case 4:
                std::cout<<"\nAktualny przebieg samochodu to: "<<ds3.przebieg<<"kilometrow";
                break;
                
            case 5:
                if (ds3.stan==0)
                {
                    std::cout<<"\nNAJPIERW URUCHOM SILNIK!!\n";
                    continue;
                }
                double km;
                std::cout<<"\nIle kilometrow masz zamiar zrobic?\n";
                std::cin>>km;
                ds3.jedz(km);
                
                break;
                
            case 6:
                if (ds3.stan==1)
                {
                    std::cout<<"NAJPIERW WYLACZ SILNIK!\n";
                    break;
                }
                double litry1;
                std::cout<<"\nW zbiorniku o pojemnosci "<<ds3.pojemnosc_baku<<" litrow znajduje sie "<<ds3.paliwo<<" litrow paliwa";
                std::cout<<"\nIle litrow chcesz zatankowac?\n";
                std::cin>>litry1;
                if((litry1+ds3.paliwo)>ds3.pojemnosc_baku)
                {
                    std::cout<<"\nTYLE SIE NIE ZMIESCI!\n";
                    break;
                }
                ds3.tankuj(litry1);
                std::cout<<"\nPo zatankowaniu w zbiorniku znajduje sie"<<ds3.paliwo<<" litrow paliwa\n";
                break;
                
            case 7:
                ds3.wylacz_silnik();
                break;
                
            case 8:
                ds3.prezentuj();
                
                break;
                
                
                
                
            default:
                std::cout<<"WYJSCIE\n";
                break;
        }
    }
    
    
    return 0;
}
